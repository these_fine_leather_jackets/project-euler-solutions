import java.util.Vector;


public class problem007 {
	public static void main(String args[]){		
		Vector<Integer> primes = new Vector<Integer>();

		int i = 1;
		while(primes.size() <= 10001) {

			boolean isPrimeNumber = true;

		    // check to see if the number is prime
			for (int j = 2; j < i; j++) {

				if (i % j == 0) {
		            isPrimeNumber = false;
					break; // exit the inner for loop
		        }
		    }    

		    // print the number if prime
		    if (isPrimeNumber) {
		        primes.add(i);
		    }
		    
		    i++;
		}
		System.out.println(primes.lastElement());
		System.out.println(primes.size());
	}
}
