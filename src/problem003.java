import java.util.Vector;


public class problem003 {
	public static void main(String args[]){
		long VALUE = 600851475143L;
		Vector<Long> factors = new Vector<Long>();
		
		
		for (long i = 2; i< VALUE; i++) {
			while(VALUE % i == 0){
				factors.add(i);
				VALUE /= i;
			}
		}
		if(VALUE > 1){
			factors.add(VALUE);
		}

		long max = 0;
		for(int j = 0; j < factors.size(); j++)
			if( factors.elementAt(j) > max)
				max = factors.elementAt(j);
		
		System.out.println(max);
	}
}
