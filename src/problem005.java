
public class problem005 {
	public static void main(String args[]){
		boolean satisfied = false;
		boolean goodsofar = true;
		long i, j;
		i = 20;
		while(!satisfied){
			System.out.println(i);
			goodsofar = true;
			for(j = 20; j >= 1; j--){
				if(i % j != 0){
					goodsofar = false;
					break;
				}
			}
			if(!goodsofar){
				i += 20;
			}
			else{
				satisfied = true;
			}
		}
		System.out.println(i);
	}
}
