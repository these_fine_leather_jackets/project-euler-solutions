
public class problem002 {
	public static void main(String args[]){
		long sum = 0;
		long j = 1;
		long k = 1;
		long swap;
		while(j+k < 4000000){
			//System.out.println(j+k);
			if((j+k) % 2 ==0)
				sum += (j+k);
			swap = j;
			j = k+j;
			k = swap;
		}			
		System.out.println(sum);
	}
}
