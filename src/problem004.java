
public class problem004 {
	public static void main(String args[]){
		int currMax = 0;
		
		for(int i = 999 ; i > 99; i--){
			for(int j = 999 ; j > 99; j--){
				System.out.println("i: " + i +" j: " + j + " i*j: " + i*j);
				if(isPalindrome(i*j)){
					if(i*j > currMax){
						currMax = i*j;
					}
				}
			}
		}
		
		System.out.println(currMax);
	}
	
	static boolean isPalindrome(int x){
		String str = String.valueOf(x);
		int i = 0;
		int j = str.length()-1;
		int midpoint;
		if(str.length() % 2 == 0)
			midpoint = str.length()/2;
		else
			midpoint = (str.length()-1)/2;	
		while(i <= midpoint){
			if(str.charAt(i) == str.charAt(j)){
				i++;
				j--;
			}
			else
				return false;
		}
		return true;
	}
}

