public class problem010 {
	public static void main(String args[]){
		final int VALUE = 2000000+1;
		boolean isprime[] = new boolean[VALUE];
		
		//init
		for(int i = 1; i < isprime.length; i++)
			isprime[i] = true;
		
		isprime[1] = false;
		
		for(int i = 2; i < isprime.length; i++){
			if(isprime[i]){
				for(int j = 2; j*i < isprime.length; j++){
					isprime[i*j] = false;
				}
			}
		}
		
		
		long sum = 0;
		for(int i = 1; i < isprime.length; i++){
			if(isprime[i] == true){
				sum += i;
				System.out.println(i);
			}
		}
		System.out.println(sum);
			
	}

}
